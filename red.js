var ws281x = require('rpi-ws281x-native');

var NUM_LEDS = parseInt(process.argv[2], 10) || 240,
    pixelData = new Uint32Array(NUM_LEDS);

ws281x.init(NUM_LEDS);

// ---- trap the SIGINT and reset before exit
process.on('SIGINT', function () {
  ws281x.reset();
  process.nextTick(function () { process.exit(0); });
});
ws281x.setBrightness(255);
/*
// ---- animation-loop
var offset = 0;
setInterval(function () {
  for (var i = 0; i < NUM_LEDS; i++) {
    pixelData[i] = colorwheel((offset + i) % 256);
  }

  offset = (offset + 1) % 256;
  ws281x.render(pixelData);
}, 1000 / 30);*/

function setRaindrop(start, end){
  var x = end;
  setInterval(function(){
		pixelData[x+1] = rgb2Int(0,0,0);
		pixelData[x+46] = rgb2Int(0,0,0);
		pixelData[x+61] = rgb2Int(0,0,0);
		pixelData[x+96] = rgb2Int(0,0,0);
		pixelData[x+111] = rgb2Int(0,0,0);
		pixelData[x+136] = rgb2Int(0,0,0);
		pixelData[x+151] = rgb2Int(0,0,0);
		pixelData[x+186] = rgb2Int(0,0,0);		
		if (x < start) { x =end;}
		pixelData[x] = rgb2Int(0,120,255);
		pixelData[x+45] = rgb2Int(0,120,255);
		pixelData[x+60] = rgb2Int(0,120,255);
		pixelData[x+95] = rgb2Int(0,120,255);
		pixelData[x+110] = rgb2Int(0,120,255);
		pixelData[x+135] = rgb2Int(0,120,255);
		pixelData[x+150] = rgb2Int(0,120,255);
		pixelData[x+185] = rgb2Int(0,120,255);
  for(var i = 30;i>27;i--) {
	pixelData[i] = rgb2Int(120,120,120);
	pixelData[i+30] = rgb2Int(120,120,120);
	pixelData[i+60] = rgb2Int(120,120,120);
	pixelData[i+120] = rgb2Int(120,120,120);
	pixelData[i+150] = rgb2Int(120,120,120);
	pixelData[i+180] = rgb2Int(120,120,120);
	pixelData[i+210] = rgb2Int(120,120,120);
	pixelData[i+240] = rgb2Int(120,120,120);
  }
  		ws281x.render(pixelData);
		x--;
	console.log(x);
  },30);

}

setRaindrop(0,29);


console.log('Press <ctrl>+C to exit.');


// rainbow-colors, taken from http://goo.gl/Cs3H0v
function colorwheel(pos) {
  pos = 255 - pos;
  if (pos < 85) { return rgb2Int(255 - pos * 3, 0, pos * 3); }
  else if (pos < 170) { pos -= 85; return rgb2Int(0, pos * 3, 255 - pos * 3); }
  else { pos -= 170; return rgb2Int(pos * 3, 255 - pos * 3, 0); }
}

function rgb2Int(r, g, b) {
  return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for(var i = 0;i < 1e7;i++){
    if((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}