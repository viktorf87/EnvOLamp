var chartAirquality;
function initializeAirquality() {


	var dpsAirquality = []; // dataPoints

	chartAirquality = new CanvasJS.Chart("airqualityChartContainer",{
			title :{
				text: "Airquality"
			},
			axisY:{
				includeZero:false,
				title:"CO2"
			},
			axisX:{
				includeZero:false,
				title:"Time"
			},
			data: [{

				type: "line",
				markerType: 'none',
				dataPoints: dpsAirquality

			}]
		});

	var xVal = 0;
	var yVal = 100;
	var updateInterval = 100;
	var dataLength = 10; // number of dataPoints visible at any point
	var i=1;
	socket.emit('getAirquality',null);

	socket.on('sendAirquality',function(airquality){
		if (typeof airquality[airquality.length -1] == 'undefined' || airquality[airquality.length -1] == "") {
			airquality.splice(airquality.length -1,1);
		}
		airquality.forEach(function(entry){
		  if (entry) {
			dpsAirquality.push({x:new Date(parseInt(entry[1])),y:parseInt(entry[0])});
			$('#actualAirquality').html(entry[0]);

		  }
		});
		chartAirquality.render();


	});


	socket.on('updateAirquality',function(airquality){
		if(chartAirquality!=null){
			dpsAirquality.push({x:new Date(parseInt(airquality[1])),y:parseInt(airquality[0])});
			i++;
			chartAirquality.render();
			$('#actualAirquality').html(airquality[0]);
		}
	});




}
