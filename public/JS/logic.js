$( document ).ready(function() {
var range_type = 'input[type=range]';
$(document).on('change', range_type, function(e) {
  var red = $("#red").val()
  var blue = $("#blue").val()
  var green = $("#green").val()
  $("#farbeAnzeigen").css("background-color","rgb(" + red + "," + green + "," + blue + ")");

  var canvas = document.getElementById('lampCanvas');
  console.log(canvas);
});

var token = '726ef72777b993532dcfc8580b9033e0c3af4a19';
spark.on('login', function() {

    //Get test event for specific core
    spark.getEventStream('someoneComing', '3a0034000a47343432313031', function(data) {
        console.log("Event: " + JSON.stringify(data));
        var someoneComing = data.data;
        console.log(someoneComing);
        if (someoneComing == 1) {
          document.getElementById("textFeld").innerHTML = "Achtung, es kommt jemand!!";
          setTimeout("someoneComing()", 5000);
        }


    });


  	    //console.log('API call completed on Login event:', body);
  	    var devicesAt = spark.getAttributesForAll();

  	    devicesAt.then(
  	        function(data){
  		//console.log('Core attrs retrieved successfully:', data);
  		    for (var i = 0; i < data.length; i++) {
  		        console.log('Device name: ' + data[i].name);
  		        console.log('- connected: ' + data[i].connected);

  			//display functions
   			if (data[i].functions == null){
  			    console.log('- functions: no functions available');
  			}
  			else {
  			    console.log('- functions: ' + data[i].functions);
  			}

  			//display variables
  			console.log('- variables: ');
  			if (data[i].variables != null) {
  		            for (variable in data[i].variables) {
  			        var type = data[i].variables[variable];
  				console.log("-- variable: " + variable + ", type: " + type);
  			    }
  			}
  			console.log("\n");
  		    }
          //--------------------------Initially get Color of lamp --------------------------
          spark.callFunction(data[0].id,'getRed', "", function(err, data) {
            if (err) {
              console.log('An error occurred:', err);
            } else {
              console.log('Function called succesfully:', data);
              red = data.return_value;
            }
          });

          spark.callFunction(data[0].id,'getGreen', "", function(err, data) {
            if (err) {
              console.log('An error occurred:', err);
            } else {
              console.log('Function called succesfully:', data);
              green = data.return_value;
            }
          });

          spark.callFunction(data[0].id,'getBlue', "", function(err, data) {
            if (err) {
              console.log('An error occurred:', err);
            } else {
              console.log('Function called succesfully:', data);
              blue = data.return_value;
              $("#farbeAnzeigen").css("background-color","rgb(" + red + "," + green + "," + blue + ")");
              $("#red").val(red);
              $("#green").val(green);
              $("#blue").val(blue);

              var canvas = document.getElementById('lampCanvas');
              console.log(canvas);
              if (canvas && canvas.getContext) {
                 var ctx = canvas.getContext("2d");
                 if (ctx) {
                    ctx.fillStyle = "rgb(" + red + "," + green + "," + blue + ")";
                    ctx.fillRect(ctx.canvas.width/2 -50,20,100, ctx.canvas.height -40);
                    ctx.fillStyle = "silver";
                    ctx.fillRect(ctx.canvas.width/2 ,20 + ctx.canvas.height - 40,6, ctx.canvas.height -20);
                  }
              }
            }
          });


          //------------------------------------------------------------------------------

$('#static').click(function(){
  spark.callFunction(data[0].id,'setRainbow', "", function(err, data) {
    if (err) {
      console.log('An error occurred:', err);
    } else {
      console.log('Function called succesfully:', data);
    }
  });
});
$('#rainbow').click(function(){
  spark.callFunction(data[0].id,'setRainbow', "", function(err, data) {
    if (err) {
      console.log('An error occurred:', err);
    } else {
      console.log('Function called succesfully:', data);
    }
  });
});
$("#getCurrentColor").click(function(){

  spark.callFunction(data[0].id,'getRed', "", function(err, data) {
    if (err) {
      console.log('An error occurred:', err);
    } else {
      console.log('Function called succesfully:', data);
      red = data.return_value;
    }
  });

  spark.callFunction(data[0].id,'getGreen', "", function(err, data) {
    if (err) {
      console.log('An error occurred:', err);
    } else {
      console.log('Function called succesfully:', data);
      green = data.return_value;
    }
  });

  spark.callFunction(data[0].id,'getBlue', "", function(err, data) {
    if (err) {
      console.log('An error occurred:', err);
    } else {
      console.log('Function called succesfully:', data);
      blue = data.return_value;
      $("#farbeAnzeigen").css("background-color","rgb(" + red + "," + green + "," + blue + ")");
      $("#red").val(red);
      $("#green").val(green);
      $("#blue").val(blue);
    }
  });
});

$("#changeColor").click(function(){

  var canvas = document.getElementById('lampCanvas');
  console.log(canvas);
  if (canvas && canvas.getContext) {
     var ctx = canvas.getContext("2d");
     if (ctx) {
        ctx.fillStyle = "rgb(" + $("#red").val() + "," + $("#green").val() + "," + $("#blue").val() + ")";
        ctx.fillRect(ctx.canvas.width/2 -50,20,100, ctx.canvas.height -40);
        ctx.fillStyle = "silver";
        ctx.fillRect(ctx.canvas.width/2 ,20 + ctx.canvas.height - 40,6, ctx.canvas.height -20);
      }
  }

spark.callFunction(data[0].id,'setRed', $("#red").val(), function(err, data) {
  if (err) {
    console.log('An error occurred:', err);
  } else {
    console.log('Function called succesfully:', data);
  }
});

spark.callFunction(data[0].id,'setGreen', $("#green").val(), function(err, data) {
  console.log($("#green").val());
  if (err) {
    console.log('An error occurred:', err);
  } else {
    console.log('Function called succesfully:', data);
  }
});

spark.callFunction(data[0].id,'setBlue', $("#blue").val(), function(err, data) {
  if (err) {
    console.log('An error occurred:', err);
  } else {
    console.log('Function called succesfully:', data);
  }
});
});
  		},
  		function(err) {
  		    console.log('API call failed: ', err);
  		}
  	    );
  	});

// Login as usual
//spark.login({ username: 'email@example.com', password: 'password'});
spark.login({ accessToken: token });

function someoneComing() {
  document.getElementById("textFeld").innerHTML = "Niemand kommt!"
}

function changeColor() {

}
});
