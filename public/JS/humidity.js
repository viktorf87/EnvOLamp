var humidityChart;
function initializeHumidity() {


	var dps = []; // dataPoints

	humidityChart = new CanvasJS.Chart("chartContainer",{
		title :{
			text: "Humidity"
		},
		axisY:{
			includeZero:false,
			title: 'humidity in %'
		},
		axisX:{
			includeZero:false,
			title:"Time"
		},
		data: [{
			type: "line",
			markerType: 'none',
			dataPoints: dps 
		}]
	});

	var xVal = 0;
	var yVal = 100;	
	var updateInterval = 100;
	var dataLength = 50; // number of dataPoints visible at any point
	var i=1;
	socket.emit('getHumidity',null);
	
	socket.on('sendHumidity',function(humidity){
		if (typeof humidity[humidity.length -1] == 'undefined' || humidity[humidity.length -1] == "") {
			humidity.splice(humidity.length -1,1);
		}
		
		humidity.forEach(function(entry){
		  if (entry) {
			dps.push({x:new Date(parseInt(entry[1])),y:parseInt(entry[0])});
			$('#actualHumidity').html(entry[0]);
		  }
		  humidityChart.render();
		});
		
	});
	
	socket.on('updateHumidity',function(humidity){
		dps.push({x:new Date(parseInt(humidity[1])),y:parseInt(humidity[0])});
		i++;
		humidityChart.render();
		$('#actualHumidity').html(humidity[0]);
	});
		

	

}