var temperatureChart;
function initializeTemperature() {


	var dpsTemperature = []; // dataPoints

	temperatureChart = new CanvasJS.Chart("temperatureChartContainer",{
		title :{
			text: "Temperature"
		},
		axisY:{
			includeZero:false,
			title: 'degree celsius'
		},
		axisX:{
			includeZero:false,
			title:"Time"
		},		
		data: [{
			type: "line",
			markerType: 'none',
			dataPoints: dpsTemperature 
		}]
	});

	var xVal = 0;
	var yVal = 100;	
	var updateInterval = 100;
	var dataLength = 50; // number of dataPoints visible at any point
	var i=1;
	socket.emit('getTemperature',null);
	
	socket.on('sendTemperature',function(temperature){
		if (typeof temperature[temperature.length -1] == 'undefined' || temperature[temperature.length -1] == "") {
			temperature.splice(temperature.length -1,1);
		}
		
		temperature.forEach(function(entry){
		  if (entry) {
			dpsTemperature.push({x:new Date(parseInt(entry[1])),y:parseInt(entry[0])});
			$('#actualTemperature').html(entry[0]);
		  }
		});
		temperatureChart.render();
	});
	
	socket.on('updateTemperature',function(temperature){
		dpsTemperature.push({x:new Date(parseInt(temperature[1])),y:parseInt(temperature[0])});
		i++;
		temperatureChart.render();
		$('#actualTemperature').html(temperature[0]);
	});
		

	

}