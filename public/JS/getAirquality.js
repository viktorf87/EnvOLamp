$(document).ready(function(){
  var myLineChart;
  var airquality = [[]];
  var socket = io();
  $("#airquality").click(function() {
     socket.emit('getAirquality', 1);
  });
  socket.on('sendAirqulity',function(msg){
    airquality = msg;
    if (typeof airquality[airquality.length -1] == 'undefined' || airquality[airquality.length -1] == "") {
      airquality.splice(airquality.length -1,1);
    }
    var dates = [];
    var values = [];
    // only want to show the last 10, but want to safe all for further investigation
    var datesSmall = [];
    var valuesSmall = [];
    airquality.forEach(function(entry){
      if (entry) {
        values[values.length] = entry[0];
        dates[dates.length] = entry[1];
      }
    });
    for(i= values.length-10;i<values.length;i++) {
      valuesSmall.push(values[i]);
      datesSmall.push(dates[i]);
    }
    //drawing the graph
    // Get the context of the canvas element we want to select
    var ctx = document.getElementById("myChart").getContext("2d");
    var data = {
      labels: datesSmall,
      datasets: [
          {
              label: "Airquality Data",
              fillColor: "rgba(220,220,220,0.2)",
              strokeColor: "rgba(220,220,220,1)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: valuesSmall
          }
      ]
  };
    myLineChart = new Chart(ctx).Line(data);

  });
  socket.on('updateAirquality',function(msg){
    if(myLineChart!=null){
		myLineChart.addData([msg[0]], msg[1]);
		myLineChart.removeData();
		myLineChart.update();
	}
  });



});
