$(document).ready(function(){
  var socket = io();
  $("#rainbow").click(function() {
     socket.emit('letItRain', 0);
  });

  $("#snow").click(function() {
     socket.emit('letItSnow', 0);
  });


  $('#canvas_picker').click(function(event){
    // getting user coordinates
    var x = event.pageX - this.offsetLeft;
    var y = event.pageY - this.offsetTop;
    // getting image data and RGB values
    var img_data = canvas.getImageData(x, y, 1, 1).data;
    var R = img_data[0];
    var G = img_data[1];
    var B = img_data[2];  var rgb = [R + ',' + G + ',' + B];
    socket.emit('changeColor',{r: R,g:G,b:B});
  });
});
