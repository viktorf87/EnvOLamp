function WeatherConnector(containerId){
  var socket = io();
	this.container = containerId;
	this.actualUrl = "http://api.openweathermap.org/data/2.5/weather?q=Constance,de&units=metric&appid=44db6a862fba0b067b1930da0d769e98";
	this.forecastUrl="http://api.openweathermap.org/data/2.5/forecast/daily?q=Constance,de&units=metric&appid=44db6a862fba0b067b1930da0d769e98";
	this.actualWeather;
	this.getActualWeather = function(){
		var result;
		$.ajax({
			url:that.actualUrl,
			success: function(data){
				var html = "<img class='weather-icon' src='http://openweathermap.org/img/w/"+data.weather[0].icon +".png' alt='"+data.weather[0].description+"'/>";
				html = '<div id="weahterEntry" class="weather-entry"><i class="owf owf-_ICONID_ owf-5x owf-pull-left "></i><strong>Today</strong><br>Temp _ACTUALTEMP_&deg;C _DESCRIPTION_</div>';
				var htmlMinMax = '<br>Min _MINTEMP_&deg;C Max _MAXTEMP_&deg;C</div>';

				html = html.replace("_ICONID_",data.weather[0].id);
				html = html.replace("_DESCRIPTION_",data.weather[0].description);
				html = html.replace("_ACTUALTEMP_",data.main.temp);
				html = html.replace("_MINTEMP_",data.main.temp_min);
				html = html.replace("_MAXTEMP_",data.main.temp_max);
				$(that.container).html(html);
				socket.emit('weatherData', data.weather[0].main);
				actualWeather = data.weather[0].main;

			},
			error: function(error){
				alert("ERROR");
			}
		});
	}

	this.GetForecast = function(){
		$.ajax({
			url:that.forecastUrl,
			success: function(data){
				var html="";
				for(var i=1; i<4;i++){
					var entry = data.list[i];
					html += '<div class="weather-entry"><i class="owf owf-_ICONID_ owf-5x owf-pull-left "></i><strong>_DATE_</strong><br>_DESCRIPTION_<br>Min _MINTEMP_&deg;C Max _MAXTEMP_&deg;C</div>';

					html = html.replace("_DATE_",getMMDDYY(entry.dt));
					html = html.replace("_ICONID_",entry.weather[0].id);
					html = html.replace("_DESCRIPTION_",entry.weather[0].description);

					html = html.replace("_MINTEMP_",entry.temp.min);
					html = html.replace("_MAXTEMP_",entry.temp.max);
					socket.emit('weatherForecastData'+i, entry.weather[0].main);
				}
				$(that.container).html(html);
			},
			error: function(error){
				alert("ERROR");
			}
		});

	}
	function getMMDDYY(ticks) {
		  var date = new Date(ticks*1000);
		  var mm = date.getMonth()+1;
		  var dd = date.getDate();
		  var yy = date.getFullYear();
		  if (mm < 10) mm = "0"+mm;
		  if (dd < 10) dd = "0"+dd;
		  return ""+dd+"."+mm+"."+yy;
	}
	var that=this;
}
