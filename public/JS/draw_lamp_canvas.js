function LampCanvas(){

	this.drawLamp = function(){
		var c = document.getElementById("myCanvas");
		drawSockel(c);


		var first_side = document.getElementById("myCanvas").getContext("2d");
		first_side.fillStyle = '#fff';
		first_side.strokeStyle="#999";
		first_side.beginPath();
		first_side.moveTo(50,100);
		first_side.lineTo(150,100);
		first_side.lineTo(150,400);
		first_side.lineTo(50,400);
		first_side.closePath();
		first_side.stroke();
		first_side.fill();

		var top= document.getElementById("myCanvas").getContext("2d");
		top.fillStyle='#fff';
		top.strokeStyle="#999";
		top.beginPath();
		top.moveTo(50,100);
		top.lineTo(100,50);
		top.lineTo(200,50);
		top.lineTo(150,100);
		top.closePath();
		top.stroke();
		top.fill();

		var s2= document.getElementById("myCanvas").getContext("2d");
		s2.strokeStyle="#999";
		s2.fillStyle='#fff';
		s2.beginPath();
		s2.moveTo(200,50);
		s2.lineTo(200,350);
		s2.lineTo(150,400);
		s2.lineTo(150,100);
		s2.closePath();
		s2.stroke();
		s2.fill();

		top.fillStyle='#00f';
		top.fill();
	}
	
	this.drawSockel =function(canvas){
		
		var front = canvas.getContext("2d");
		front.fillStyle="#FABF34";
		front.strokeStyle="#F2B82E";
		front.beginPath();
		front.moveTo(20,410);
		front.lineTo(170,410);
		front.lineTo(170,430);
		front.lineTo(20,430);
		front.closePath();
		front.stroke();
		front.fill();

		var top = canvas.getContext("2d");
		top.fillStyle="#FABF34";
		top.strokeStyle="#F2B82E";
		top.beginPath();
		top.moveTo(20,410);
		top.lineTo(100,335);
		top.lineTo(245,335);
		top.lineTo(170,410);
		top.closePath();
		top.stroke();
		top.fill();

		var right =  canvas.getContext("2d");
		right.fillStyle="#FABF34";
		right.strokeStyle="#F2B82E";
		right.beginPath();
		right.moveTo(170,430);
		right.lineTo(245,355);
		right.lineTo(245,335);
		right.lineTo(170,410);
		right.closePath();
		right.stroke();
		right.fill();
	}
}

