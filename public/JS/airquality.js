var token = '726ef72777b993532dcfc8580b9033e0c3af4a19';
spark.on('login', function() {
    //Get test event for specific core
    spark.getEventStream('someoneComing', '3a0034000a47343432313031', function(data) {
        console.log("Event: " + JSON.stringify(data));
    });


  	    //console.log('API call completed on Login event:', body);
  	    var devicesAt = spark.getAttributesForAll();

  	    devicesAt.then(
  	        function(data){
  		//console.log('Core attrs retrieved successfully:', data);
  		    for (var i = 0; i < data.length; i++) {
  		        console.log('Device name: ' + data[i].name);
  		        console.log('- connected: ' + data[i].connected);

  			//display functions
   			if (data[i].functions == null){
  			    console.log('- functions: no functions available');
  			}
  			else {
  			    console.log('- functions: ' + data[i].functions);
  			}

  			//display variables
  			console.log('- variables: ');
  			if (data[i].variables != null) {
  		            for (variable in data[i].variables) {
  			        var type = data[i].variables[variable];
  				console.log("-- variable: " + variable + ", type: " + type);
  			    }
  			}
  			console.log("\n");
  		    }

          spark.callFunction(data[0].id,'getT', "", function(err, data) {
            if (err) {
              console.log('An error occurred:', err);
            } else {
              console.log('Function called succesfully:', data);
              console.log(data);
            }
          });

          spark.callFunction(data[0].id,'getHumidity', "", function(err, data) {
            if (err) {
              console.log('An error occurred:', err);
            } else {
              console.log('Function called succesfully:', data);
              console.log(data);
            }
          });

          spark.callFunction(data[0].id,'getPPM', "", function(err, data) {
            if (err) {
              console.log('An error occurred:', err);
            } else {
              console.log('Function called succesfully:', data);
              console.log(data);
            }
          });






  		},
  		function(err) {
  		    console.log('API call failed: ', err);
  		}
  	    );
  	});

    // Login as usual
//spark.login({ username: 'email@example.com', password: 'password'});
spark.login({ accessToken: token });
