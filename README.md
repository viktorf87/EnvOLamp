EnvOLamp

This is a project we made for the seminar Physical Computing at the University of Konstanz.
To run this project you need the following components:
- Hardware
   - Rasperry Pi
   - Adafruit Neo Pixels (we used 8 meters (240 pixels)
- Software
   - Node.js has to be installed on the Raspberry Pi. 
      - Please don't install the arm version of Node.js     
	  
Please run server.js with admin permissions
   - sudo node server.js