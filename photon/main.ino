// This #include statement was automatically added by the Particle IDE.
#include "Adafruit_DHT/Adafruit_DHT.h"
// This #include statement was automatically added by the Particle IDE.
#include "MQ135/MQ135.h"
#include "math.h"
// This #include statement was automatically added by the Particle IDE.
#define DHTTYPE DHT11   // DHT 11
#define DHTPIN D7
int qualitaet[10000];
int qualitaetData[100];
//--------------------------Gas Sensor------------------------------------------------
int number = 0;
int state = 0;
MQ135 gasSensor = MQ135(A0);
float rzero = gasSensor.getRZero();
int ppm = gasSensor.getPPM();
#define PARA 116.6020682
#define PARB 2.769034857
//END--------------------------Gas Sensor------------------------------------------------
int temperature;
int humidity;
DHT dht(DHTPIN, DHTTYPE);
void setup() {
Serial.begin(9600);
Particle.function("getT", getT);
Particle.function("getPPM", getPPM);
Particle.function("getHumidity", getHumidity);
/*for(int j=0;j< 100 ;j++) {
strip.setPixelColor(j,255,0,0);
strip.show();
}
for(int j=0;j< 100 ;j++) {
strip2.setPixelColor(j,255,0,0);
strip2.show();
}
for(int j=0;j< 100 ;j++) {
strip3.setPixelColor(j,255,0,0);
strip3.show();
}*/
dht.begin();
}
void loop() {
float rzero = gasSensor.getRZero();                      // stop the program for some time
int co2_ppm = gasSensor.getPPM();
int ppm = co2_ppm / 4;
//Particle.variable("qData", qualitaetData);
int qualitaetSum = 0;
for(int i=0;i<10000;i++) {
qualitaet[i] = getPPM("hi");
qualitaetSum += qualitaet[i];      
}
qualitaetSum = qualitaetSum /10000;
/*if(qualitaetSum > 900) {
for(int j=0;j< 100 ;j++) {
strip.setPixelColor(j,255,0,0);
strip2.setPixelColor(j,255,0,0);
strip3.setPixelColor(j,255,0,0);
}
strip.show();
strip2.show();
strip3.show();
}*/
//Serial.println(qualitaetSum);
float h = dht.getHumidity();
humidity = (int) h;
// Read temperature as Celsius (the default)
float t = dht.getTempCelcius();
temperature = (int) t;
// Read temperature as Fahrenheit (isFahrenheit = true)
//float f = dht.getTemperature(true);
// Check if any reads failed and exit early (to try again).
// if (isnan(h) || isnan(t) || isnan(f)) {
//     Serial.println("Failed to read from DHT sensor!");
//     return;
//   }
// Compute heat index in Fahrenheit (the default)
//float hif = dht.computeHeatIndex(f, h);
// Compute heat index in Celsius (isFahreheit = false)
//float hic = dht.computeHeatIndex(t, h, false);
/*
Serial.print(f);
Serial.print(" *F\t");
Serial.print("Heat index: ");
Serial.print(hic);
Serial.print(" *C ");
Serial.print(hif);
Serial.println(" F"); /
}
int getT(String x) {
return temperature;
}
int getPPM(String x) {
float value = PARA * pow((gasSensor.getResistance()/65), -PARB);
int newValue = (int)value;
return newValue;
}
int getHumidity(String x ) {
return humidity;
}