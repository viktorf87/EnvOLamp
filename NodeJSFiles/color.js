var ws281x = require('rpi-ws281x-native');

var NUM_LEDS = parseInt(process.argv[2], 10) || 240,
    pixelData = new Uint32Array(NUM_LEDS);

ws281x.init(NUM_LEDS);

// ---- trap the SIGINT and reset before exit
process.on('SIGINT', function () {
  ws281x.reset();
  process.nextTick(function () { process.exit(0); });
});
function changeColor(color){
    // for(var x = 0;x<240;x++) {
    //   pixelData[x] = rgb2Int(0,0,0);
    // }
    for(var i = 0;i<30;i++) {
      for(var x = 0;x<8;x++) {
        pixelData[i+x*30] = rgb2Int(color[0],color[1],color[2]);
      }
    }
    ws281x.render(pixelData);

}

var flashCounter = 1;
var flashSet = false;
function flashChangeColor(color) {
  if(flashCounter < 10) {
    var r = color[0];
    if(r-50+(flashCounter-4)*10 < 0) {r = 0} else {r = r-50+(flashCounter-4)*10}
    var g = color[1];
    if(g-50+(flashCounter-4)*10 < 0) {g = 0} else {g = g-50+(flashCounter-4)*10}
    var b = color[2];
    if(b-50+(flashCounter-4)*10 < 0) {b = 0} else {b = b-50+(flashCounter-4)*10}
    for(var i = 0;i<30;i++) {
      for(var x = 0;x<8;x++) {
        pixelData[i+x*30] = rgb2Int(r,g,b);
        //pixelData[29*4] = rgb2Int(255,255,255);
      }
    }
    ws281x.render(pixelData);
    flashCounter ++;
    flashSet = true;
  }
  flashCounter ++;
  if(flashCounter > 10) {
    for(var x = 0;x<240;x++) {
      pixelData[x] = rgb2Int(0,0,0);
    }
    ws281x.render(pixelData);
  }
}

function smoothChangeColor(color){
     for(var x = 0;x<240;x++) {
       pixelData[x] = rgb2Int(0,0,0);
     }
    for(var i = 0;i<30;i++) {
      for(var x = 0;x<8;x++) {
        pixelData[i+x*30] = rgb2Int(color[0],color[1],color[2]);
   	//pixelData[29*4] = rgb2Int(255,255,255);
      }
      sleep(10);
      ws281x.render(pixelData);
    }
}
var pulseCounter = 1;
function pulseColor(color) {
    if (pulseCounter == 10){
      pulseCounter = 1;
    } else if (pulseCounter > 4) {
      var r = color[0];
      if(r-50+(pulseCounter-4)*10 < 0) {r = 0} else {r = r-50+(pulseCounter-4)*10}
      var g = color[1];
      if(g-50+(pulseCounter-4)*10 < 0) {g = 0} else {g = g-50+(pulseCounter-4)*10}
      var b = color[2];
      if(b-50+(pulseCounter-4)*10 < 0) {b = 0} else {b = b-50+(pulseCounter-4)*10}
        for(var i = 0;i<30;i++) {
          for(var x = 0;x<8;x++) {
            pixelData[i+x*30] = rgb2Int(r,g,b);
          }
        }
        pulseCounter++;
        //console.log('r: '+r + 'g: '+g + 'b: ' + b);
        ws281x.render(pixelData);
      } else if (pulseCounter < 5) {
        var r = color[0];
        if(r-10*pulseCounter < 0) {r = 0} else {r = r-10*pulseCounter}
        var g = color[1];
        if(g-10*pulseCounter < 0) {g = 0} else {g = g-10*pulseCounter}
        var b = color[2];
        if(b-10*pulseCounter < 0) {b = 0} else {b = b-10*pulseCounter}
        for(var i = 0;i<30;i++) {
          for(var x = 0;x<8;x++) {
            pixelData[i+x*30] = rgb2Int(r,g,b);
          }
        }
        pulseCounter++;
        ws281x.render(pixelData);
        //console.log('r: '+r + 'g: '+g + 'b: ' + b);
      }
  }

function rgb2Int(r, g, b) {
      return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for(var i = 0;i < 1e7;i++){
      if((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
}

exports.setColor = function(color) {
  changeColor(color);
}
exports.reset1 = function() {
  for(var i = 0;i<30;i++) {
    for(var x = 0;x<8;x++) {
      pixelData[i+x*30] = rgb2Int(0,0,0);
      //pixelData[29*4] = rgb2Int(255,255,255);
    }
  }
  flashCounter = 1;
  flashSet = false;
  pulseCounter = 1;
}
exports.smoothSetColor = function(color) {
  smoothChangeColor(color);
}

exports.pulseColor = function(color) {
  pulseColor(color);
}

exports.flashChangeColor = function(color) {
  flashChangeColor(color);
}
