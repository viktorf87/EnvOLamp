var ws281x = require('rpi-ws281x-native');
var NUM_LEDS = parseInt(process.argv[2], 10) || 240,
    pixelData = new Uint32Array(NUM_LEDS);

ws281x.init(NUM_LEDS);
var end = 0;

function Snowflake(start,timeToLife){
      this.oldStart = start;
      this.counter = start;
      this.timeToLifeOld = timeToLife;
      this.stripe = [0,0,0,0,0,0,0,0];
      this.timeLast= 0;
      this.timeToLife = timeToLife;
      ws281x.render(pixelData);
}

Snowflake.prototype.makeSideSnow = function(newDate,side) {
  this.timeNow = newDate;
  if(this.counter == this.oldStart) {
      this.counter = getRandomInt(this.oldStart,this.oldStart-5);
  }


  if(this.timeNow-this.timeLast>100) {
          pixelData[(this.counter+(side*30))+1] = rgb2Int(0,0,0);
          pixelData[(this.counter+((side+1)*30))+1] = rgb2Int(0,0,0);
          pixelData[this.counter+(side*30)] = rgb2Int(255,255,255);
          pixelData[this.counter+((side+1)*30)] = rgb2Int(255,255,255);
    this.counter--;
    ws281x.render(pixelData);
    this.timeLast = this.timeNow;
    //this.timeToLife--;
  }
  if(this.counter == end) {
    for(var x = 0;x<8;x++) {
      if(this.stripe[x]) {
        pixelData[(this.counter+1+(side*30))] = rgb2Int(0,0,0);
        pixelData[(this.counter+1+((side+1)*30))] = rgb2Int(0,0,0);
      }
    }
    //this.timeToLife = this.timeToLifeOld;
    this.counter = getRandomInt(this.oldStart-5,this.oldStart);
  }
}

Snowflake.prototype.makeSnow = function(newDate,start,end) {
      this.timeNow = newDate;
      if(this.counter == this.oldStart) {
        for(var i = 0;i<8;i++) {
          this.stripe[i] = getRandomInt(0,1);
        }
        this.counter = getRandomInt(start,end);
      }


      if(this.timeNow-this.timeLast>100) {
          for(var x = 0;x<8;x++) {
            if(this.stripe[x]) {
              pixelData[(this.counter+(x*30))+1] = rgb2Int(0,0,0);
              pixelData[this.counter+(x*30)] = rgb2Int(255,255,255);
            }
          }
        this.counter--;
        ws281x.render(pixelData);
        this.timeLast = this.timeNow;
        //this.timeToLife--;
      }
      if(this.counter == end) {
        for(var x = 0;x<8;x++) {
          if(this.stripe[x]) {
            pixelData[(this.counter+1+(x*30))] = rgb2Int(0,0,0);
          }
        }
        //this.timeToLife = this.timeToLifeOld;
        this.counter = getRandomInt(start-5,start);
        for(var i = 0;i<8;i++) {
          this.stripe[i] = getRandomInt(0,1);
        }
      }
}

//--------------Raindrop--------------------------------------
function Raindrop(start,timeToLife){
      this.oldStart = start;
      this.counter = start;
      this.timeToLifeOld = timeToLife;
      this.stripe = [0,0,0,0,0,0,0,0];
      this.timeLast= 0;
      this.timeToLife = timeToLife;
      ws281x.render(pixelData);
}

Raindrop.prototype.makeSideRain = function(newDate,side) {
  this.timeNow = newDate;
  if(this.counter == this.oldStart) {
      this.counter = getRandomInt(this.oldStart,this.oldStart-5);
  }


  if(this.timeNow-this.timeLast>100) {
          pixelData[(this.counter+(side*30))+1] = rgb2Int(0,0,0);
          pixelData[(this.counter+((side+1)*30))+1] = rgb2Int(0,0,0);
          pixelData[this.counter+(side*30)] = rgb2Int(0,0,255);
          pixelData[this.counter+((side+1)*30)] = rgb2Int(0,0,255);
    this.counter--;
    ws281x.render(pixelData);
    this.timeLast = this.timeNow;
    //this.timeToLife--;
  }
  if(this.counter == end) {
    for(var x = 0;x<8;x++) {
      if(this.stripe[x]) {
        pixelData[(this.counter+1+(side*30))] = rgb2Int(0,0,0);
        pixelData[(this.counter+1+((side+1)*30))] = rgb2Int(0,0,0);
      }
    }
    //this.timeToLife = this.timeToLifeOld;
    this.counter = getRandomInt(this.oldStart-5,this.oldStart);
  }
}

Raindrop.prototype.makeRain = function(newDate,start,end) {
      this.timeNow = newDate;
      if(this.counter == this.oldStart) {
        for(var i = 0;i<8;i++) {
          this.stripe[i] = getRandomInt(0,1);
        }
      }


      if(this.timeNow-this.timeLast>50) {
          for(var x = 0;x<8;x++) {
            if(this.stripe[x]) {
              pixelData[(this.counter+(x*30))+1] = rgb2Int(0,0,0);
              pixelData[this.counter+(x*30)] = rgb2Int(0,0,255);
            }
          }
        this.counter--;
        ws281x.render(pixelData);
        this.timeLast = this.timeNow;
        //this.timeToLife--;
      }
      if(this.counter == end) {
        for(var x = 0;x<8;x++) {
          if(this.stripe[x]) {
            pixelData[(this.counter+1+(x*30))] = rgb2Int(0,0,0);
          }
        }
        //this.timeToLife = this.timeToLifeOld;
        this.counter = getRandomInt(start-5,start);
        for(var i = 0;i<8;i++) {
          this.stripe[i] = getRandomInt(0,1);
        }
      }
}
//-----------------------------------------------------------------------------



function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var lastDate = 0;
var lastDate1 = 0;
var lastDate2 = 0;
var snowObject = new Snowflake(30,30);
var snowObject1 = new Snowflake(30,30);
var snowObject2 = new Snowflake(30,30);
var rainObject = new Raindrop(30,30);
var rainObject1 = new Raindrop(30,30);
var rainObject2 = new Raindrop(30,30);

exports.clouds = function() {
  for(var x = 0;x<240;x++) {
    pixelData[x] = rgb2Int(120,120,120);
  }
  ws281x.render(pixelData);
}

exports.sideClouds = function(side) {
  for(var x = 0;x<30;x++) {
    pixelData[x+(side*30)] = rgb2Int(120,120,120);
    pixelData[x+((side+1)*30)] = rgb2Int(120,120,120);
  }
  ws281x.render(pixelData);
}

exports.sun = function() {
  for(var x = 0;x<240;x++) {
    pixelData[x] = rgb2Int(255,255,0);
  }
  ws281x.render(pixelData);
}

exports.sideSun = function(side) {
  for(var x = 0;x<30;x++) {
    pixelData[x+(side*30)] = rgb2Int(255,255,0);
    pixelData[x+((side+1)*30)] = rgb2Int(255,255,0);
  }
  ws281x.render(pixelData);
}

exports.rain = function() {
  var getDate = new Date()
  var timespan = getDate.getTime()-lastDate;
  var timespan1 = getDate.getTime()-lastDate1;
  var timespan2 =getDate.getTime()-lastDate2;
  if(timespan - 20 > 0) {
    rainObject.makeRain(getDate.getTime(),30,0);
    lastDate = getDate.getTime();
  }
  if (timespan1- 20 >0) {
    rainObject1.makeRain(getDate.getTime(),30,0);
    lastDate1 = getDate.getTime();
  }
  if (timespan2 - 20 > 0) {
    rainObject2.makeRain(getDate.getTime(),30,0);
    lastDate2 = getDate.getTime();
  }
}
exports.snow = function() {
  var getDate = new Date()
  var timespan = getDate.getTime()-lastDate;
  var timespan1 = getDate.getTime()-lastDate1;
  var timespan2 =getDate.getTime()-lastDate2;
  if(timespan - 20 > 0) {
    snowObject.makeSnow(getDate.getTime(),30,0);
    lastDate = getDate.getTime();
  }
  if (timespan1- 40 >0) {
    snowObject1.makeSnow(getDate.getTime(),30,0);
    lastDate1 = getDate.getTime();
  }
  if (timespan2 - 60 > 0) {
    snowObject2.makeSnow(getDate.getTime(),30,0);
    lastDate2 = getDate.getTime();
  }
}

exports.sideSnow = function(side) {
  var getDate = new Date()
  var timespan = getDate.getTime()-lastDate;
  var timespan1 = getDate.getTime()-lastDate1;
  var timespan2 =getDate.getTime()-lastDate2;
  if(timespan - 20 > 0) {
    snowObject.makeSideSnow(getDate.getTime(),side);
    lastDate = getDate.getTime();
  }
  if (timespan1- 40 >0) {
    snowObject1.makeSideSnow(getDate.getTime(),side);
    lastDate1 = getDate.getTime();
  }
  if (timespan2 - 60 > 0) {
    snowObject2.makeSideSnow(getDate.getTime(),side);
    lastDate2 = getDate.getTime();
  }
}

exports.sideRain = function(side) {
  var getDate = new Date()
  var timespan = getDate.getTime()-lastDate;
  var timespan1 = getDate.getTime()-lastDate1;
  var timespan2 =getDate.getTime()-lastDate2;
  if(timespan - 20 > 0) {
    rainObject.makeSideRain(getDate.getTime(),side);
    lastDate = getDate.getTime();
  }
  if (timespan1- 40 >0) {
    rainObject1.makeSideRain(getDate.getTime(),side);
    lastDate1 = getDate.getTime();
  }
  if (timespan2 - 60 > 0) {
    rainObject2.makeSideRain(getDate.getTime(),side);
    lastDate2 = getDate.getTime();
  }
}


    // rainbow-colors, taken from http://goo.gl/Cs3H0v
function colorwheel(pos) {
    pos = 255 - pos;
    if (pos < 85) { return rgb2Int(255 - pos * 3, 0, pos * 3); }
    else if (pos < 170) { pos -= 85; return rgb2Int(0, pos * 3, 255 - pos * 3); }
    else { pos -= 170; return rgb2Int(pos * 3, 255 - pos * 3, 0); }
}

function rgb2Int(r, g, b) {
      return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for(var i = 0;i < 1e7;i++){
      if((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
}
