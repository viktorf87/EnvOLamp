var ws281x = require('rpi-ws281x-native');

var NUM_LEDS = parseInt(process.argv[2], 10) || 240,
    pixelData = new Uint32Array(NUM_LEDS);

ws281x.init(NUM_LEDS);

// ---- trap the SIGINT and reset before exit
process.on('SIGINT', function () {
  ws281x.reset();
  process.nextTick(function () { process.exit(0); });
});
ws281x.setBrightness(255);


onmessage = function(e) {
    console.log('es schenit');
    var snow = true;
    while(snow) {
      console.log('es schenit');
      letSnow()
    }
    postMessage({ test : 'this is a test' });
};

function setRaindrop(){
  raindrop(getRandomInt(1,8));
  console.log(getRandomInt(1,8));
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function raindrop(stripeNumbers) {
  for (var i = stripeNumber*30;i>(stripeNumber*30)-30;i--) {
    pixelData[i+1] = rgb2Int(0,0,0);
    pixelData[i] = rgb2Int(0,0,255);
    ws281x.render(pixelData);
    pixelData[i] = rgb2Int(0,0,0);
  }
}

function letSnow() {
  console.log('es schneit');
  var stripe = [0,0,0,0,0,0,0,0];
  for(var i = 0;i<8;i++) {
    stripe[i] = getRandomInt(0,1);
  }
  for (var i = 29;i>=0;i--) {
    for(var x = 0;x<8;x++) {
      if(stripe[x]) {
        pixelData[(i+((x+1)*30))+1] = rgb2Int(0,0,0);
        pixelData[i+((x+1)*30)] = rgb2Int(120,0,0);
        ws281x.render(pixelData);
      }
    }
    for(var x = 0;x<8;x++) {
      pixelData[i+((x+1)*30)] = rgb2Int(0,0,0);
    }
  }
}



exports.rain = function(state) {
  if(state) {
    setRaindrop();
  }
}
exports.snow = function(state) {
  if(state) {
    snow();
  }
}


    // rainbow-colors, taken from http://goo.gl/Cs3H0v
function colorwheel(pos) {
    pos = 255 - pos;
    if (pos < 85) { return rgb2Int(255 - pos * 3, 0, pos * 3); }
    else if (pos < 170) { pos -= 85; return rgb2Int(0, pos * 3, 255 - pos * 3); }
    else { pos -= 170; return rgb2Int(pos * 3, 255 - pos * 3, 0); }
}

function rgb2Int(r, g, b) {
      return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for(var i = 0;i < 1e7;i++){
      if((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
}

var signals = {
  'SIGINT': 2,
  'SIGTERM': 15
};


function shutdown(signal, value) {
  console.log('Stopped by ' + signal);
  lightsOff();
  process.nextTick(function () { process.exit(0); });
}

Object.keys(signals).forEach(function (signal) {
  process.on(signal, function () {
    shutdown(signal, signals[signal]);
  });
});
