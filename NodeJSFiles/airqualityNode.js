//------------------------airquality-----------------------------------
var spark = require('spark');
var fs = require('fs')
var token = '726ef72777b993532dcfc8580b9033e0c3af4a19';
spark.on('login', function() {
    //Get test event for specific core
  	    //console.log('API call completed on Login event:', body);
  	    var devicesAt = spark.getAttributesForAll();
  	    devicesAt.then(
  	        function(data){
  		},
  		function(err) {
  		    console.log('API call failed: ', err);
  		}
  	    );
  	});

    // Login as usual
//spark.login({ username: 'email@example.com', password: 'password'});
spark.login({ accessToken: token });
//connects to the photon cloud and gets the ppm value which the photon has right now and saves it in the Array ariquality
function getPPMNative(airquality) {
  //request ppm from photon
  var devicesAt = spark.getAttributesForAll();
  var airquality = airquality;

  devicesAt.then(
      function(data){
    spark.callFunction(data[0].id,'getPPM', "", function(err, data) {
      if (err) {
        console.log('An error occurred:', err);
      } else {
        console.log('Function called succesfully:', data);
        var date = new Date();

        var year = date.getUTCFullYear();
        var month = date.getUTCMonth();
        var day = date.getUTCDate();
        var hours = date.getUTCHours();
        var minutes = date.getUTCMinutes();
        var seconds = date.getUTCSeconds();

        //month 2 digits
        month = ("0" + (month + 1)).slice(-2);

        //year 2 digits
        year = year.toString().substr(2,2);

        //var formattedDate = hours + '/' + minutes + "/" + seconds + "//" + day + '/' + month + "/" + year ;
        var formattedDate = Date.now();
		// update graph

        //--------------------
        airquality[airquality.length] = ['' + data.return_value,formattedDate];
        for(i=0;i<airquality.length;i++) {
          if(airquality[i] == undefined || airquality[i] == 0 || airquality[i] == '') {
            delete airquality[i];
          }
        }
        // save the Data in an external file
        var file = fs.createWriteStream('/home/pi/Documents/EnvOLamp/airqualitLog1');
        file.on('error', function(err) { /* error handling */ });
        airquality.forEach(function(v) {
        if(v == undefined) {
          airquality.delete(v);
        }
         if (v != "") {
           file.write(v.join(',') + '\n');
         }
        });
        file.end();

    console.log("The file was saved!");
      }
    },
    function(err) {
        console.log('API call failed: ', err);
    }
      );
    });
    return airquality;
}

exports.getPPM = function(airquality) {
  return getPPMNative(airquality);
}
//enables possibility to add additional stribes to a photon
exports.setLight = function() {
  setLight();
  console.log('geschafft');
}
//enables possibility to add additional stribes to a photon
function setLight() {
  //request ppm from photon
  var devicesAt = spark.getAttributesForAll();

  devicesAt.then(
      function(data){
    spark.callFunction(data[0].id,'setLight', "99:3:4:5", function(err, data) {
      if (err) {
        console.log('An error occurred:', err);
      } else {
        console.log('Function called succesfully:', data);
      }
    },
    function(err) {
        console.log('API call failed: ', err);
    }
      );
    });
}
