var ws281x = require('rpi-ws281x-native');
var NUM_LEDS = parseInt(process.argv[2], 10) || 240,
    pixelData = new Uint32Array(NUM_LEDS);

ws281x.init(NUM_LEDS);


function rgb2Int(r, g, b) {
      return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
}

function showProgress(oldTime, time, progressColor) {
  this.oldTime = oldTime;
  var passedMinutes = passedTimeMins(this.oldTime);
  if (passedMinutes < 0.001) {
    for(var x = 0;x<240;x++) {
      pixelData[x] = rgb2Int(0,0,0);
    }
  }
  console.log(passedMinutes);
   for(var x = 0;x<30 *(passedMinutes/time);x++) {
     for(var i = 0;i<8;i++) {
        pixelData[x+(i*30)] = rgb2Int(progressColor[0],progressColor[1],progressColor[2]);
     }
   }
  ws281x.render(pixelData);
  if(passedMinutes > time) {
    return true;
  }
  return false;
}
exports.passedTime = function(oldTime) {
  return passedTimeMins(oldTime);
}

function passedTimeMins (oldTime) {
  var time = new Date().getTime();
  var passedTime = time - oldTime;
  var timePassedMinutes = (passedTime/1000);
  return timePassedMinutes;
}

exports.showProgress = function(oldTime, progressTime, progressColor){
  var finished = showProgress(oldTime, progressTime, progressColor);
  if(finished == true) {
    return true;
  }
  return false;
}
exports.reset1 = function() {
  for(var i = 0;i<30;i++) {
    for(var x = 0;x<8;x++) {
      pixelData[i+x*30] = rgb2Int(0,0,0);
    }
  }
}