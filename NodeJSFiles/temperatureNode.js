//------------------------temperature-----------------------------------
var spark = require('spark');
var fs = require('fs')
var token = '726ef72777b993532dcfc8580b9033e0c3af4a19';

spark.on('login', function() {
    //Get test event for specific core
  	    //console.log('API call completed on Login event:', body);
  	    var devicesAt = spark.getAttributesForAll();
  	    devicesAt.then(
  	        function(data){
  		},
  		function(err) {
  		    console.log('API call failed: ', err);
  		}
  	    );
  	});


spark.login({ accessToken: token });
//connects to the photon cloud and gets the ppm value which the photon has right now and saves it in the Array ariquality

function getTemperatureNative(temperature) {
  //request ppm from photon
  var devicesAt = spark.getAttributesForAll();
  var temperature = temperature;

  devicesAt.then(
      function(data){
    spark.callFunction(data[0].id,'getT', "", function(err, data) {
      if (err) {
        console.log('An error occurred:', err);
      } else {
        console.log('Function called succesfully:', data);
        var date = new Date();

        var year = date.getUTCFullYear();
        var month = date.getUTCMonth();
        var day = date.getUTCDate();
        var hours = date.getUTCHours();
        var minutes = date.getUTCMinutes();
        var seconds = date.getUTCSeconds();

        //month 2 digits
        month = ("0" + (month + 1)).slice(-2);

        //year 2 digits
        year = year.toString().substr(2,2);

        //var formattedDate = year + '-' + month + '-' +day + " " + hours + ':' + minutes + ":" + seconds;
        var formattedDate = Date.now();
		// update graph

        //--------------------
        temperature[temperature.length] = ['' + data.return_value,formattedDate];
        for(i=0;i<temperature.length;i++) {
          if(temperature[i] == undefined || temperature[i] == 0 || temperature[i] == '') {
            delete temperature[i];
          }
        }
        // save the Data in an external file
        var file = fs.createWriteStream('/home/pi/Documents/EnvOLamp/temperatureLog');
        file.on('error', function(err) { /* error handling */ });
        temperature.forEach(function(v) {
        if(v == undefined) {
          temperature.delete(v);
        }
         if (v != "") {
           file.write(v.join(',') + '\n');
         }
        });
        file.end();

    console.log("The file was saved!");
      }
    },
    function(err) {
        console.log('API call failed: ', err);
    }
      );
    });
    return temperature;
}

exports.getTemperature = function(temperature) {
  return getTemperatureNative(temperature);
}
