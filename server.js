//--------------WebSocket-------------------------------------------------------------------
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs')
var lampState = 'none';
var makeRain;
var weather = require('./NodeJSFiles/weather.js');
var airqulityNode = require('./NodeJSFiles/airqualityNode.js');
var humidityNode = require('./NodeJSFiles/humidityNode.js');
var temperatureNode = require('./NodeJSFiles/temperatureNode.js');
var changeColor = require('./NodeJSFiles/color.js');
var progress = require('./NodeJSFiles/progress.js');
var oldTime;
var CO2Min = 400;
var CO2Max = 700;
var temperatureMax = 26;
var temperatureMin = 16;
var humidityMax = 30;
var humidityMin = 20;
var lampColor = {r:50,g:50,b:50};
var progressTime = 0;
var progressColor = {r:0,g:0,b:0};
var progressColorObject ={r:50,g:50,b:50};
var progressPassedTime = 0;
var weahterState;
var weatherForecastData1;
var weatherForecastData2;
var weatherForecastData3;
var lastDate = 0;
var updateAirquality;
var updateIntervall = 30000;
var lampPower = false;

var scorePlayer1 = 0;
var scorePlayer2 = 0;

var path = require('path');
app.use(express.static(__dirname+ "/public"));

//--------------------------------read airqualitLog1----------------------------------------------
var airqualityFile = fs.readFileSync('../EnvOLamp/airqualitLog1','utf8');
var airqualityRes = airqualityFile.split("\n");
var airquality = [];
airqualityRes.forEach(function(entry){
  airquality[airquality.length] = entry.split(",");
});
//------------------------------------------------------------------------------------------------
//--------------------------------read humidityLog------------------------------------------------
var humidityFile = fs.readFileSync('../EnvOLamp/humidityLog','utf8');
var humidityRes = humidityFile.split("\n");
var humidity = [];
humidityRes.forEach(function(entry){
  humidity[humidity.length] = entry.split(",");
});
//------------------------------------------------------------------------------------------------
//--------------------------------read temperatureLog------------------------------------------------
var temperatureFile = fs.readFileSync('../EnvOLamp/temperatureLog','utf8');
var temperatureRes = temperatureFile.split("\n");
var temperature = [];
temperatureRes.forEach(function(entry){
  temperature[temperature.length] = entry.split(",");
});
//------------------------------------------------------------------------------------------------
//weather.rain(true);


io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('getSettings', function() {
    socket.emit('sendSettings', {
                                  Power: lampPower,
                                  LampState: lampState,
                                  LightColor: lampColor,
                                  WorkprogressColor: progressColorObject,
                                  WorkProgressMinutes: progressTime,
                                  WorkProgressRemaining: progressTime-progressPassedTime,
                                  CO2Min: CO2Min,
                                  CO2Max: CO2Max,
                                  TempMin: temperatureMin,
                                  TempMax: temperatureMax,
                                  HumMin: humidityMin,
                                  HumMax: humidityMax
                                });
  });

  //------------------------try unity connection
  socket.on('goalLeft', function(msg){
    scorePlayer1 ++;
    console.log('Player1: ' + scorePlayer1);
    changeColor.reset1();
    lampState = "flash1";
  });

  socket.on('goalRight', function(msg){
    scorePlayer2 ++;
    console.log('Player2: ' + scorePlayer2);
    changeColor.reset1();
    lampState = "flash2";
  });

  socket.on('reset', function(msg){
    scorePlayer1 = 0;
    scorePlayer2 = 0;
    console.log('reseted');
  });

  socket.on('flash', function(msg) {
    console.log("flash");
    changeColor.reset1();
    lampState = 'flash1';
  });
  //---------------------------------
  socket.on('changeColor', function(msg){
    changeColor.smoothSetColor([msg.r,msg.g,msg.b]);
    lampColor = {r:msg.r,g:msg.g,b:msg.b};
    lampState = "color";
  });
  socket.on('getAirquality', function(msg){
    socket.emit('sendAirquality', airquality.slice(airquality.length-10));
  });

  socket.on('getHumidity',function(msg){
	   socket.emit('sendHumidity',humidity.slice(humidity.length-10));
  });

  socket.on('getTemperature',function(msg){
	   socket.emit('sendTemperature',temperature.slice(temperature.length-10));
  });

  socket.on('letItRain', function(msg){

    console.log('wetter ist regen');

    if (lampState != 'rain') {
        lampState = 'rain';
    } else {
        lampState = 'none';
        changeColor.setColor([0,0,0]);
        console.log('cleared');
    }
  });
  socket.on('letItSnow', function(msg){
    if (lampState != 'snow') {
        lampState = 'snow';
    } else {
        lampState = 'none';
        changeColor.setColor([0,0,0]);
        console.log('cleared');
    }
  });
  socket.on('letTheSunShine', function(msg){
	//TODO: SHOW THE SUN
  });

  socket.on('showTemperature', function(msg){
	   if(lampState != 'temperature') {
       lampState = 'temperature';
       temperatureMax = msg.Max;
       temperatureMin = msg.Min;
	   showTemperatureValue();
     } else {
       lampState = 'none';
       changeColor.setColor([0,0,0]);
       console.log('cleared');
     }
  });

  socket.on('showHumidity', function(msg){
     if(lampState != 'humidity') {
       lampState = 'humidity';
       humidityMax = msg.Max;
       humidityMin = msg.Min;
	   showHumidityValue();
     } else {
       lampState = 'none';
       changeColor.setColor([0,0,0]);
       console.log('cleared');
     }
  });

  socket.on('showAirquality', function(msg){
    if (lampState != 'airquality') {
        lampState = 'airquality';
		console.log("CO2MAXVALUE: " + msg.Max);
        CO2Max = msg.Max;
        CO2Min = msg.Min;
		showAirqualityValue();

    } else {
        lampState = 'none';
        changeColor.setColor([0,0,0]);
        console.log('cleared');
    }
  });

  socket.on('setPower',function(msg) {
    if (msg) {
      lampPower = true;
      lampState = 'color';
      lampColor = {r:252,g:199,b:39};
      changeColor.setColor([252,199,39]);
    } else {
      lampPower = false;
      lampState = 'none';
    }
  });

  socket.on('weatherData', function(msg){
    switch (msg) {
      case 'Clouds':
       weatherState = 'Clouds';
       break;
      case 'Thunderstorm':
        weatherState = 'Rain';
        break;
      case 'Drizzle':
        weatherState = 'Rain';
        break;
      case 'Rain':
        weatherState = 'Rain';
        break;
      case 'Snow':
        weatherState = 'Snow';
        break;
      case 'Atmosphere':
        weatherState = 'Sun';
        break;
      case 'Clear':
        weatherState = 'Sun';
        break;
      case 'Clouds':
        weahterState = 'Clouds';
        break;
      case 'Extreme':
        weatherState = 'Rain';
        break;
      default: weatherState = 'Sun';
    }
    console.log(msg);
  });
  socket.on('weatherForecastData1', function(msg){
    weatherForecastData1 = msg;
    console.log(msg);
  });
  socket.on('weatherForecastData2', function(msg){
    weatherForecastData2 = msg;
    console.log(msg);
  });
  socket.on('weatherForecastData3', function(msg){
    weatherForecastData3 = msg;
    console.log(msg);
  });
  socket.on('weatherForecast', function(msg){
    weatherState = "forecast";
    console.log(msg);
  });

  socket.on('showWeather', function(msg){
    lampState = weatherState;
  });

  socket.on('workProgress', function(msg){
    oldTime = (new Date().getTime());
    progressTime = msg.time;
    console.log(msg);
    progressColor = [msg.progressColor.r,msg.progressColor.g,msg.progressColor.b];
    progressColorObject = {r:msg.progressColor.r,g:msg.progressColor.g,b:msg.progressColor.b};
    console.log(msg.time);
    if (lampState != 'progress') {
        lampState = 'progress';
    } else {
        lampState = 'none';
        changeColor.smoothSetColor([0,0,0]);
        console.log('cleared');
    }
  });
  setInterval(function() {
  socket.emit('updateAirquality', airquality[airquality.length -1]);
	socket.emit('updateHumidity',humidity[humidity.length -1]);
	socket.emit('updateTemperature',temperature[temperature.length -1]);

  },updateIntervall);
});

// updates airquality and writes it into the airqualitLog1 File
setInterval(function(){
  airquality = airqulityNode.getPPM(airquality);
  humidity = humidityNode.getHumidity(humidity);
  temperature = temperatureNode.getTemperature(temperature);
  console.log('gemacht');
},updateIntervall);

setInterval(draw,1000/30);

//update function which sets the Lamp to its momentaneous state
var blinkCounter = 0;
var colorState;
var blinkDone = false;
function draw() {
  switch(lampState) {
    case 'none':
      var done = false;
      if (!done) {
        changeColor.setColor([0,0,0]);
        done = true;
      }
      break;
      //flashing with unity
    case "flash1":
      changeColor.flashChangeColor([255,0,0]);
      break;
    case "flash2":
      changeColor.flashChangeColor([0,0,255]);
      break;
      //-------------------------------------

    case "Rain":
        weather.rain();
      break;
    case "Snow":
        weather.snow();
      break;
    case "Clouds":
      var done = false;
      if (!done) {
        weather.clouds();
		done = true;
      }
      break;
    case "Sun":
      var done = false;
      if (!done) {
        weather.sun();
        done = true;
      }
      break;
    case "forecast":
      weather.forecast();

      break;
    case "blink":
    if(blinkCounter == 0 ) {changeColor.reset1()}
    var getDate = new Date()
    var timespan = getDate.getTime()-lastDate;
    if(timespan > 100) {
      if(blinkCounter < 40) {
        changeColor.pulseColor(progressColor);
        lastDate = getDate.getTime();
        blinkCounter ++;
      } else {
        if (!blinkDone) {
          changeColor.reset1();
          changeColor.setColor([252,199,39]);
          blinkDone = true;
      }
    }
  }
      break;
    case "progress":
    //console.log(progressTime);
      var finished = progress.showProgress(oldTime,progressTime,progressColor);
      console.log(finished);
      progressPassedTime = progress.passedTime(oldTime);
      if (finished) {
        lampState = 'blink';
        colorState = 'progressColor';
        oldCounterTime = new Date().getTime();
        blinkCounter = 0;
		progress.reset1();
		blinkDone=false;
      }
      break;
    case "airquality":
    // lets the lamp show the value of the airquality, good means blue, bad means brown
      var getDate = new Date()
      var timespan = getDate.getTime()-lastDate;
      if(timespan > 10000) {
        showAirqualityValue();
        lastDate = getDate.getTime();
      }
      break;
    case 'humidity':
    // lets the lamp show the value of the humiditu, too low means yellow, too high means blue
      var getDate = new Date()
      var timespan = getDate.getTime()-lastDate;
      if(timespan > 10000) {
        showHumidityValue();
        lastDate = getDate.getTime();
      }
      break;
    case 'temperature':
    // lets the lamp show the value of the humiditu, too low means blue, too high means red/orange
      var getDate = new Date()
      var timespan = getDate.getTime()-lastDate;
      if(timespan > 10000) {
        showTemperatureValue();
        lastDate = getDate.getTime();
      }
      break;
  }
}

function showAirqualityValue(){
	var ppmArray = airquality[airquality.length -1];
	if(ppmArray == '') {
	  airquality.splice(airquality.length -1,1);
	  ppmArray = airquality[airquality.length -1];
	}
	var ppmValue = parseInt(ppmArray[0]);
	if (typeof ppmValue == 'undefined' || ppmValue == "") {
	  airquality.splice(airquality.length -1,1);
	  ppmArray = airquality[airquality.length -1];
	  ppmValue = parseInt(ppmArray[0]);
	}
	if (ppmValue > CO2Max) {
	  var rgb = [120,101,45];
	}
	else if(ppmValue > CO2Min){
	  var diffValue = ppmValue - CO2Min;
	  var rgb = [parseInt(0+(Math.round((120/(CO2Max-CO2Min)) * 10) /10)*diffValue),parseInt(191-((Math.round((90/(CO2Max-CO2Min)) * 10) /10)*diffValue)),parseInt(255-((Math.round((210/(CO2Max-CO2Min)) * 10) /10)*diffValue))];
	}
	else {
	  var rgb = [0,191,255];
	}
	changeColor.setColor(rgb);
  console.log(rgb);
  console.log(ppmValue);
}

function showHumidityValue(){
	var humidityArray = humidity[humidity.length -1];
	if(humidityArray == '') {
	  humidity.splice(humidity.length -1,1);
	  humidityArray = humidity[humidity.length -1];
	}
	var humidityValue = parseInt(humidityArray[0]);
	if (typeof humidityValue == 'undefined' || humidityValue == "") {
	  humidity.splice(humidity.length -1,1);
	  humidityArray = humidity[humidity.length -1];
	  humidityValue = parseInt(humidityArray[0]);
	}
	console.log("HUMIDITYVALUE: " + humidityValue);
  if (humidityValue > humidityMax) {
	  var rgb = [120,101,45];
	}
	if(humidityValue > humidityMin){
	  var diffValue = humidityValue - humidityMin;
	  var rgb = [parseInt(255-((Math.round((255/(humidityMax-humidityMin)) * 10) /10)*diffValue)),parseInt(255-((Math.round((150/(humidityMax-humidityMin)) * 10) /10)*diffValue)),parseInt(0+((Math.round((255/(humidityMax-humidityMin)) * 10) /10)*diffValue))];
	  console.log(rgb);
	} else {
	  var rgb = [255,255,0];
	}
	changeColor.setColor(rgb);
}
function showTemperatureValue(){
	var temperatureArray = temperature[temperature.length -1];
	if(temperatureArray == '') {
	  temperature.splice(temperature.length -1,1);
	  temperatureArray = temperature[temperature.length -1];
	}
	var temperatureValue = parseInt(temperatureArray[0]);
	if (typeof temperatureValue == 'undefined' || temperatureValue == "") {
	  temperature.splice(temperature.length -1,1);
	  temperatureArray = temperature[temperature.length -1];
	  temperatureValue = parseInt(temperatureArray[0]);
	}
  if (temperatureValue > temperatureMax) {
    var rgb = [255,80,0];
  }
	if(temperatureValue > temperatureMin){
	  var diffValue = temperatureValue - temperatureMin;
	  var rgb = [parseInt(0+((Math.round((255/(temperatureMax-temperatureMin)) * 10) /10)*diffValue)),parseInt(0+((Math.round((80/(temperatureMax-temperatureMin)) * 10) /10)*diffValue)),parseInt(255-((Math.round((255/(temperatureMax-temperatureMin)) * 10) /10)*diffValue))];
	  console.log(rgb);
	} else {
	  var rgb = [0,0,255];
	}
	changeColor.setColor(rgb);
}
//Listen to the Port
http.listen(8082, function(){
  console.log('listening on *:8082');
});
//-----------------------------------------------------------------------------------------------------------------
